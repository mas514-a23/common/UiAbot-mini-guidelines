Welcome to MAS514's documentation!
===================================

.. image:: src/fig/uiabot.jpg
 
.. toctree::
   :maxdepth: 2
   :caption: Introduction:

   src/exercises
   src/simulink_installation
   src/ros2humble

.. toctree::
   :maxdepth: 2
   :caption: Lab Exercises:

   src/lab_exercise1_hints
   src/lab_exercise2_hints

.. toctree::
   :maxdepth: 2
   :caption: Technical:
   
   src/pinout
   src/motor_data
Motors
======

Motor Data
----------
The motors mounted on the UiAbot Mini have the specifications highlighted in the table below:

.. figure:: fig/motor_table.jpg

Motor Dimensions
----------------------
.. figure:: fig/motor_dimensions.jpg

Encoder Data
------------
.. figure:: fig/encoder_wiring.jpg
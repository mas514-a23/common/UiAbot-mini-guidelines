Simulink Installation
=====================

Installation and Setup
----------------------

Follow the steps below to install the necessary software to communicate between Simulink and UiA Bot Mini

.. TIP::
    Do the installation process in the order that it is written, or else you might run into trouble.

1. Install/update to Matlab 2023a
2. Install USB Serial driver using this link https://www.silabs.com/documents/public/software/CP210x_Windows_Drivers.zip
3. Open Simulink, press the "Apps" tab --> Get Add-Ons
4. In the Add-Ons menu, search for "Arduino"
5. Install Arduino Hardware package for Simulink
6. When you are asked to connect your board, select ESP32-WROOM-DevkitC and run the test. 

.. admonition:: Important!
   :class: error

    Don't worry if the test fails, it will be fine.

Once the Arduino Hardware package for Simulink is installed, go to the "Hardware" tab in the Simulink window. 

.. attention::
    **If you don't see the "Hardware" tab**, go to the "Apps" tab, then press the dropdown showing all installed apps. Lastly, press the "Run on Hardware Board" button.
    Wait for a few seconds, and the "Hardware" tab should now be shown.

Next press the "Hardware settings" button, and select "Hardware Implementation" in the menu. Make sure the "Hardware Board" is set to ESP32-WROOM.

Then press the "Target hardware resources" drop down and go to "Host-board connection". Set the host COM port to "Manually Specify" and put your COM port number.
If you don't know your COM port number, press the Windows button on your computer and search for "Device Manager". Then go to the dropdown menu "Ports (COM & LPT)" and find the number of your COM port.

After completing these steps, you should be able to communicate between Simulink and your UiAbot Mini.

Verify the installation
-----------------------
To verify the installation, you can open a Simulink project, double click on the white area and search for Arduino Digital Output.
If you can find it, then you are ready to start testing the features of the UiAbot. You need the following blocks to control the motor and read the encoder:

- Digital Output
- Arduino PWM
- Arduino Rotary Encoder
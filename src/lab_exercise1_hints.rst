Lab Exercise 1 Hints
====================

Encoder convertion factor
-------------------------
If you are struggeling to get the output from the encoder converted correctly, a solution is provided in the hidden dropdown below.

.. container:: toggle, toggle-hidden

    .. admonition:: Hint

        .. image:: fig/encoder_hint.png
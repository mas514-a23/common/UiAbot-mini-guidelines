Lab Exercise 2 Hints
====================

Before running the ``Simulator.slx`` file, you need to enter the following command into Matlab Command Window: ``sldrtkernel -install``. You will need to reboot your computer afterwards.
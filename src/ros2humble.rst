Installing ROS2 Humble from source
==================================

.. admonition:: Important!
   :class: error

    Since the Jetson Nano on the UiAbot Mini comes with an Ubuntu 20.04 image preinstalled, you need to install
    ROS2 Humble from source (with Ubuntu 22.04 you can install it from apt).

    Before following the steps at https://docs.ros.org/en/humble/Installation/Alternatives/Ubuntu-Development-Setup.html,
    please run the following two commands in the terminal of you Jetson Nano:

    .. code-block:: bash

        sudo apt-get purge -y pybind11-dev
        python3 -m pip install -U pybind11-global

    When you have entered those two commands successfully, you can follow the installation guide provided by ROS.

Estimated build time with Jetson Nano power mode set to MAXN (0): 3h 30min.